const express = require('express');
const router = express.Router();

const userController = require('./user.controller');

router.get('/:userId', userController.get_user_by_Id);

router.post('/signup', userController.create_user);

router.post('/login', userController.user_login);

router.delete('/:userId', userController.delete_user);

module.exports = router;