const User = require('./user.model');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const settings = require('../../config');

exports.get_user_by_Id = async (req, res, next) => {
    const id = req.params.userId;
    console.log(id);
    const users = await User.findById(id)
    .select('_id name email');
    res.status(200).json({
        data: users
    });
};

exports.create_user = async (req, res, next) => {
    const user = await User.find({email: req.body.email})
        if (user.length >= 1) {
            return res.status(409).json({
                message: 'this email already exists'
            });
        } else {
            bcrypt.hash(req.body.password, 10, async (err, hash) => {
            if (err){
                return res.status(500).json({
                    error: err
                });
            } else {
                const user = new User({
                    _id: new mongoose.Types.ObjectId(),
                    email: req.body.email,
                    password: hash,
                    name: req.body.name
                });
                const result = await user.save();
                res.status(201).json(result);
            }
        });
    }
};

exports.user_login = async (req, res, next) => {
       const user = await User.find({email: req.body.email});
       
            if (user.length < 1){
                return res.status(401).json({
                    message: 'incorrect email or password'
                });
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: 'Auth failed'
                    });
                }
                if (result) {
                    const token = jwt.sign({
                        email: user[0].email,
                        userId: user[0]._id
                    },
                    settings.JWT_KEY, 
                    {
                        expiresIn: '1h'
                    },
                    );
                    return res.status(200).json({
                        message: 'Auth successful',
                        token: token
                    });
                }
                return res.status(401).json({
                    message: 'incorrect password or password'
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}


exports.delete_user = async (req, res, next) => {
    const id = req.params.userId;
    const users = await User.remove({_id: id})
    res.status(200).json({
        message: 'User deleted',
    });
};

