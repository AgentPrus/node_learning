const express = require('express');
const router = express.Router();

const categoryController = require('./category.controller')

router.get('/', categoryController.category_get_all);

router.post('/', categoryController.category_post);

router.get('/:categoryId', categoryController.category_get_by_id);

router.patch('/:categoryId', categoryController.category_patch);

router.delete('/:categoryId', categoryController.category_delete);

module.exports = router;
