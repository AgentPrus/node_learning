const mongoose = require('mongoose');

const categorySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId, // random generate id
    name: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Category', categorySchema);