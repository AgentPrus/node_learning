const Category = require('./category.model');
const mongoose = require('mongoose');



exports.category_get_all = async (req, res, next) => {
    const category = await Category.find().select('name _id')
        res.status(200).json({
             data: category
        });
};

exports.category_post = async (req, res, next) => {
    const category = new Category({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name
});
    const categories = await category.save()
        res.status(201).json({
            message: 'Created category successfully',
            createdCategory: {
                name: categories.name,
                _id: categories._id,
         }
     });
};

exports.category_get_by_id = async (req, res, next) => {
    const id = req.params.categoryId;
    console.log(id)
    const categories = await Category.findById(id)
    .select('name _id')

    res.status(200).json({
        data: categories
    });
};

exports.category_patch = async (req, res, next) => {
    const id = req.params.categoryId;
    const updateOps = {};
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    const categories = await Category.update({_id: id }, { $set: updateOps})
    res.status(200).json({
        message: 'Category updated',
        request: {
            type: 'GET',
            url: 'http://localhost:8000/category/' + id
        }
    });
};

exports.category_delete = async (req, res, next) => {
    const id = req.params.categoryId;
    const category = await Category.remove({_id: id})
    res.status(200).json({
        message: 'Category deleted',
    });
};