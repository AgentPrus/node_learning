const Artical = require('./artical.model');
const mongoose = require('mongoose')

exports.artical_get_all = async (req, res, next) => {
    const artical = await Artical.find().select('title text _id').populate('categoryId')
    res.status(200).json({
        articles: artical
    });
};

exports.artical_get_by_id = async (req, res, next) => {
    const id = req.params.articalId;
    console.log(id)
    const categories = await Artical.findById(id)
    .select('title _id text')

    res.status(200).json({
        data: categories
    });
};

exports.artical_post = async (req, res, next) => {
    const artical = new Artical({
        _id: new mongoose.Types.ObjectId(),
        categoryId: req.body.categoryId,
        title: req.body.title,
        text: req.body.text 
    });
    const articals = artical.save()
    res.status(201).json({
        message: 'Created artical successfully',
        artical: artical
    });
};

exports.artical_patch = async (req, res, next) => {
    const id = req.params.articalId;
    const updateOps = {};
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    const articals = await Artical.update({_id: id }, { $set: updateOps})
    res.status(200).json({
        message: 'Artical updated',
        request: {
            type: 'GET',
            url: 'http://localhost:8000/artical/' + id
        }
    });
};

exports.artical_delete = async (req, res, next) => {
    const id = req.params.articalId;
    const articals = await Artical.remove({_id: id})
    res.status(200).json({
        message: 'Artical deleted',
    });
};