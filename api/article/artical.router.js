const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Artical = require('./artical.model');
const Category = require('../category/category.model');

const ArticlesController = require('../article/artical.controller'); 

router.get('/', ArticlesController.artical_get_all);

router.get('/:articalId', ArticlesController.artical_get_by_id);

router.post('/', ArticlesController.artical_post);

router.patch('/:articalId', ArticlesController.artical_patch);

router.delete('/:articalId', ArticlesController.artical_delete);

module.exports = router;