const express = require('express');
const blog = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const categoryRoutes = require('./api/category/category.router');
const articalRoutes = require('./api/article/artical.router');
const userRoutes = require('./api/user/user.router');

blog.use(morgan('dev'));
blog.use(bodyParser.urlencoded({extended: false}));
blog.use(bodyParser.json());

blog.use((req, res, next) => {
    res.header('Access-Control-Allow-Oigin', '*');
    res.header('Access-Control-Allow-Headers',
     'Origin, X-Requested-With, Content-Type, Accept, Authorization');
     if(req.method === 'OPTIONS'){
         res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
         return res.status(200).json({});
     }
     next();
});

blog.use('/category', categoryRoutes);
blog.use('/article', articalRoutes);
blog.use('/user', userRoutes);

mongoose.connect(
    'mongodb://localhost/Our_Awesome_Hebron_Soft_Blog',
    {
        useCreateIndex: true,
        useNewUrlParser: true
    }, () => {
        console.log('Connect to MongoDb...');
    }
);

module.exports = blog;

