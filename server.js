const http = require('http');
const blog = require('./blog');

const port = process.env.PORT||8000;

const server = http.createServer(blog);

server.listen(port);