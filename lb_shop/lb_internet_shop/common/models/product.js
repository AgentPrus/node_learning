'use strict';

module.exports = function(Product) {
    Product.observe('after save', async (ctx, next) => {
        const id = ctx.instance.id;
        const CardItem = Product.app.models.CardItem;
        const card_item = await CardItem.find({
            include: {
                relation: 'product',
            },
            where: {
                productId: id
            }
        });
        
        card_item.forEach(element => {
            element.save();
        });
    });
};
