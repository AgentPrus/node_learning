'use strict';
const app = require('../../server/server');

module.exports = function (Card) {
    Card.getInfo = async (cardId) => {
        const getCard = await Card.findOne({
            include: {
                relation: 'cardsItems',
            },
            where: { id: cardId }
        });
        return getCard;
    },

        Card.createOrder = async (cardId) => {
            const getCard = await Card.findOne({
                include: {
                    relation: 'cardsItems',
                },
                where: { id: cardId }
            });
            const order = await Card.app.models.Order.findOne({
                where: { userId: getCard.userId }
            });

            getCard.cardsItems().forEach(async (cardItem) => {
                cardItem.cardId = null;
                cardItem.orderId = order.id;
                await cardItem.save();
            });

        }

    Card.remoteMethod(
        'createOrder',
        {
            description: 'ordered items',
            http: { path: '/createOrder', verb: 'post' },
            accepts: { arg: 'cardId', type: 'string', required: true, http: { source: 'query' } },
            returns: { arg: 'card', type: 'object' }
        }
    );

    Card.remoteMethod(
        'getInfo',
        {
            description: 'show info about cards',
            http: { path: '/getInfo', verb: 'get' },
            accepts: { arg: 'cardId', type: 'string', required: true, http: { source: 'query' } },
            returns: { arg: 'card', type: 'object' }
        }
    );
};
