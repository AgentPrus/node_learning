'use strict';

module.exports = function (Carditem) {
    Carditem.observe('before save', async (ctx, next) => {
        const Product = Carditem.app.models.Product;
        if (ctx.instance) {
            const id = ctx.instance.productId;
            const product = await Product.findById(id); // дістаю об'єкт
            ctx.instance.totalSum = product.price * ctx.instance.quantity;
        }else if(ctx.data && ctx.data.quantity){
            const id = ctx.currentInstance.productId;
            const product = await Product.findById(id); // дістаю об'єкт
            ctx.data.totalSum = product.price * ctx.data.quantity   
        }
    });

    Carditem.observe('after save', async (ctx, next) => {
        const Card = Carditem.app.models.Card;
        if (ctx.instance) {
            const card = await Card.findOne({ // знаходжу об'єкти які є в card
                where: { id: ctx.instance.cardId },
                include: 'cardsItems'
            });
            const cartItems = await card.cardsItems.find({}); // поміщаю їх в змінну

            card.totalSum = 0;
            for (let i = 0; i < cartItems.length; i++) { // рахую їх суму 
                card.totalSum += cartItems[i].totalSum
            }
            console.log(card.totalSum)
            await card.save();
        }

    });

    Carditem.observe('before delete', async (ctx, next) => {
        const cardItem = await Carditem.findById(ctx.where.id); // находжу card-item
        const Card = Carditem.app.models.Card;
        const card = await Card.findById(cardItem.cardId); // знаходжу Card в якому лежить об'єкт card-item
        card.totalSum -= cardItem.totalSum;
        await card.save();
    })
};