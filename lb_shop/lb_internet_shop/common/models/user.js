'use strict';
const app = require('../../server/server');

module.exports = function (User) {
    User.afterRemote('create', async (ctx, user, next) => {
        const Card = app.models.Card;
        await Card.create({
            totalSum: 0,
            userId: user.id
        });
    });
};
